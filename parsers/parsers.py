from jobblob import JobblobParser
from pingwest import PingWestParser
from gankfun import GankfunParser
from infoq import InfoQParser
from yinwang import YinWangParser
from valleytalk import ValleyTalkParser
from zhizhihu import ZhiZhiHuParser

parser_dict = {'http://blog.jobbole.com/' : JobblobParser,
               'http://www.pingwest.com/' : PingWestParser,
               'http://www.infoq.com/cn/' : InfoQParser,
               'http://www.yinwang.org/' : YinWangParser,
               'http://www.geekfan.net/' : GankfunParser,
               'http://www.valleytalk.org/' : ValleyTalkParser,
               'http://www.zhizhihu.com/' : ZhiZhiHuParser}
           
website_list = ['http://www.yinwang.org/',
                'http://www.geekfan.net/',
                'http://www.pingwest.com/',
                'http://www.zhizhihu.com/',
                'http://www.valleytalk.org/',
                'http://www.infoq.com/cn/',
                'http://blog.jobbole.com/',
                ]

if __name__ == "__main__":
    parser = GankfunParser()
    parser.parse_items()
    parser.show_items()