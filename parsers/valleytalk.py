from common import WebsiteParser, get_tag_value, valid_tag

class ValleyTalkParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.valleytalk.org/')
        self.is_a = False
        self.is_h1 = False
        self.href = ''
    def start_h1(self, attrs):
        self.is_h1 = True
    def end_h1(self, attrs):
        self.is_h1 = False
    def start_a(self, attrs):
        if not self.is_h1:
            return
        self.href = get_tag_value(attrs, 'href')[0]
        self.is_a = True
    def end_a(self):
        self.is_a = False
    def handle_data(self, text):
        if self.is_a:
            self.append_item(text, self.href)