from common import WebsiteParser, get_tag_value, valid_tag, tag_not_exist

class PingWestParser1(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.pingwest.com/')
        self.is_a = False
        self.is_h1 = False
        self.is_p = False
        self.href = ''
        self.title = ''
    def start_a(self, attrs):
        if valid_tag(attrs, 'target', '_blank')\
            and tag_not_exist(attrs, 'class'):
            self.href = get_tag_value(attrs, 'href')[0]
            self.is_a = True
    def end_a(self):
        self.is_a = False
        self.is_h1 = False
    def start_h1(self, attrs):
        if not self.is_a:
            return
        if valid_tag(attrs, 'class', 'article-title'):
            self.is_h1 = True
    def start_p(self, attrs):
        if not self.is_h1:
            return
        if valid_tag(attrs, 'class', 'excerpt'):
            self.is_p = True
            self.is_h1 = False
    def end_p(self):
        self.is_p = False
    def handle_data(self, text):
        if self.is_h1 and not self.is_p:
            self.title = text
        if not self.is_p:
            self.append_item(self.title, self.href, text)
            
class PingWestParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.pingwest.com/')
        self.is_a = False
        self.is_h1 = False
        self.href = ''
    def start_a(self, attrs):
        if valid_tag(attrs, 'target', '_blank'):
            self.href = get_tag_value(attrs, 'href')[0]
            self.is_a = True
    def end_a(self):
        self.is_a = False
    def start_h1(self, attrs):
        if not self.is_a:
            return
        if valid_tag(attrs, 'class', 'article-title'):
            self.is_h1 = True
    def end_h1(self):
        self.is_h1 = False
    def handle_data(self, text):
        if self.is_h1:
            self.append_item(text, self.href)