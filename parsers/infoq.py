from common import WebsiteParser, get_tag_value, valid_tag

class InfoQParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.infoq.com/cn/')
        self.is_a = False
        self.is_h3 = False
        self.href = ''
    def start_h3(self, attrs):
        self.is_h3 = True
    def end_h3(self, attrs):
        self.is_h3 = False
    def start_a(self, attrs):
        if not self.is_h3:
            return
        #value = get_tag_value(attrs, 'title')
        #if value:#valid_tag(attrs, 'title', ''):
        #    print value
        self.href = get_tag_value(attrs, 'href')[0]
        self.is_a = True
    def end_a(self):
        self.is_a = False
    def handle_data(self, text):
        if self.is_a:
            self.append_item(text, self.href)