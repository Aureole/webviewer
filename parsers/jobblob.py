from common import WebsiteParser, get_tag_value, valid_tag

class JobblobParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://blog.jobbole.com/')
        self.is_a = False
        self.href = ''
    def start_a(self, attrs):
        if valid_tag(attrs, 'class', 'meta-title'):
            self.href = get_tag_value(attrs, 'href')[0]
            self.is_a = True
    def end_a(self):
        self.is_a = False
    def handle_data(self, text):
        if self.is_a:
            self.append_item(text, self.href)