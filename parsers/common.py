import sys, urllib2  
from sgmllib import SGMLParser   
      
def get_url_content(url):      
    headers = {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'}  
    req = urllib2.Request(url, headers=headers)  
    content = urllib2.urlopen(req).read()   # UTF-8  
    return content
    type = sys.getfilesystemencoding()      # local encode format  
    return content.decode("UTF-8").encode(type)  # convert encode format           

def get_tag_value(attrs, key):
    return [v for k, v in attrs if k == key]
    
def valid_tag(attrs, tag, value):
    tag_value = get_tag_value(attrs, tag)
    if value and value in tag_value:
        return True
    return False
    
def tag_not_exist(attrs, tag):
    return not get_tag_value(attrs, tag)

class WebsiteItem():
    def __init__(self, title, href, excerpt = ''):
        self.title = title
        self.href = href
        self.excerpt = excerpt
    def to_string(self):
        return 'title: ' + self.title + '\n' \
             + 'href: ' + self.href + '\n' \
             + 'excerpt: ' + self.excerpt + '\n'\
             + '\n'
    
class WebsiteParser(SGMLParser):
    def __init__(self, url, code = 'utf-8'):
        SGMLParser.__init__(self)
        self.content = ''
        self.url = url
        self.hrefs = set()
        self.items = []
        self.code = code
    def parse_items(self):
        self.content = get_url_content(self.url)
        self.feed(self.content)
    def _decode_item(self, item):
        #return item
        #local_code = sys.getfilesystemencoding()
        return item.decode(self.code)#.encode(local_code)
    def append_item(self, title, href, excerpt = ''):
        if self._item_duplicated(href):
            return
        self.items.append(WebsiteItem(self._decode_item(title),\
                href, self._decode_item(excerpt)))
        self.hrefs.add(href)
    def show_items(self):
        for item in self.items:
            print item.to_string()
    def _item_duplicated(self, href):
        return href in self.hrefs
    def items_to_string(self):
        string = ''
        for item in self.items:
            string += item.to_string()
        return string