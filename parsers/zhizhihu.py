from common import WebsiteParser, get_tag_value, valid_tag

class ZhiZhiHuParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.zhizhihu.com/')
        self.is_a = False
        self.is_h3 = False
        self.href = ''
    def start_h3(self, attrs):
        self.is_h3 = True
    def end_h3(self, attrs):
        self.is_h3 = False
    def start_a(self, attrs):
        if not self.is_h3:
            return
        self.href = get_tag_value(attrs, 'href')[0]
        self.is_a = True
    def end_a(self):
        self.is_a = False
    def handle_data(self, text):
        if self.is_a:
            self.append_item(text, self.href)