from common import WebsiteParser, get_tag_value

class YinWangParser(WebsiteParser):
    def __init__(self):
        WebsiteParser.__init__(self, 'http://www.yinwang.org/')
        self.is_a = False
        self.href = ''
    def start_a(self, attrs):
        self.href = get_tag_value(attrs, 'href')[0]
        self.is_a = True
    def end_a(self):
        self.is_a = False
    def handle_data(self, text):
        if self.is_a:
            self.append_item(text, self.href)