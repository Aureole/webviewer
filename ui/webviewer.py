from PyQt4 import QtGui, QtCore
from PyQt4 import QtWebKit
from gui import Ui_MainWindow
import parsers.parsers as parsers

def generate_website_item(website):
    item = QtGui.QStandardItem(website)
    item.setEditable(False)
    return item

def generate_href_item(href):
    item = QtGui.QStandardItem(href)
    item.setEditable(False)
    return item  

class ParserThread(QtCore.QThread):
    signal = QtCore.pyqtSignal(str)
    def __init__(self, website_item_dict, parent = None):
        QtCore.QThread.__init__(self)
        self.id = id
        self.running = False
        self.item_dict = website_item_dict
        self.to_stop = False
        self.parent = parent
        if parent:
            self._connect_slots()
    def _connect_slots(self):
        self.signal.connect(self.parent.set_footer)
    
    def _update_parent_state(self, string):
        if self.parent:
            self.signal.emit(string)
        
    def run(self):
        self.running = True
        self._update_parent_state('parse start.')
        for website in parsers.website_list:
            print website
            self._update_parent_state('parsing ' + website)
            if self.to_stop:
                break
            parser = parsers.parser_dict[website]()
            try:
                parser.parse_items()
            except:
                self._update_parent_state('parse ' + website + ' fail.')
                continue
            self.item_dict[website] = parser.items
            self._update_parent_state('parse ' + website + 'finish.')
        self._update_parent_state('parse finish.')
        self.running = False
    def stop(self):
        self.to_stop = True
    
class WebsiteViewer(QtGui.QMainWindow):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self.progress = 0
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self._connect_web_slots()
        self._connect_slots()
        self._init_web_list()
        self._init_item_list()
        self.show()
        self.items = {}
        self.parser = None
        self._refresh()

    def _connect_web_slots(self):
        self.ui.webView.page().setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
        self.ui.button_foward.clicked.connect(self.ui.webView.forward)
        self.ui.button_back.clicked.connect(self.ui.webView.back)
        self.ui.button_reload.clicked.connect(self.ui.webView.reload)
        self.ui.button_stop.clicked.connect(self.ui.webView.stop)
        self.ui.locationEdit.returnPressed.connect(self._change_location)
        self.ui.webView.linkClicked.connect(self._load_url)
        self.ui.webView.loadProgress.connect(self._set_progress)
        self.ui.webView.loadFinished.connect(self._adjust_location)
        self.ui.webView.loadFinished.connect(self._finish_loading)
        self.ui.webView.titleChanged.connect(self._adjust_title)
    def _connect_slots(self):
        self.ui.button_refresh.clicked.connect(self._refresh)
        self.ui.listView_item.doubleClicked.connect(self._show_content)
        self.ui.listView_website.clicked.connect(self._show_items)
    def _init_web_list(self):
        self.ui.listView_website.setStyleSheet(
            "QListView::item{height: 30px;}"
            )
        self.model_web = QtGui.QStandardItemModel(self)
        self.ui.listView_website.setModel(self.model_web)
        for website in parsers.website_list:
            web_item = generate_website_item(website.split('/')[2])
            self.model_web.appendRow(web_item)
    def _init_item_list(self):
        self.ui.listView_item.setStyleSheet(
            "QListView::item{height: 30px;}"
            )
        self.model_item = QtGui.QStandardItemModel(self)
        self.ui.listView_item.setModel(self.model_item)
    '''   
    net wokring
    '''
    def _load_url(self, url, is_qurl = True):
        if is_qurl:
            self.ui.webView.load(url)
        else:
            self.ui.webView.load(QtCore.QUrl(url))
        self.ui.webView.setFocus()
    def _set_progress(self, progress):
        self.progress = progress
        self._adjust_title()
    def _adjust_location(self):
        self.ui.locationEdit.setText(self.ui.webView.url().toString())
    def _adjust_title(self):
        if self.progress <= 0 or self.progress >= 100:
            self.setWindowTitle(self.ui.webView.title())
        else:
            self.setWindowTitle(QtCore.QString("%1 (%2%)").arg(self.ui.webView.title()).arg(self.progress))
    def _finish_loading(self):
        self.progress = 100
        self._adjust_title()
    def _change_location(self):
        address = self.ui.locationEdit.text()
        if address.indexOf ('http://') == -1:
            address = 'http://' + address
        self._load_url(QtCore.QUrl(address))

    def _show_content(self, index):
        self._load_url(self.cur_items[index.row()].href, False)
    def _show_items(self, index):
        self.model_item.clear()
        website = parsers.website_list[index.row()]
        if website not in self.items:
            return
        self.cur_items = self.items[website]
        for item in self.cur_items:
            web_item = generate_website_item(unicode(item.title))
            self.model_item.appendRow(web_item)
            
    def set_footer(self, string):
        self.ui.footer.setText(string)        
        
    def _refresh(self):
        if self.parser != None and self.parser.running:
            return
        self.parser = ParserThread(self.items, self)
        self.parser.start()


 